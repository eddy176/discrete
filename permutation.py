def permutation_gen(nums):
    n = len(nums)
    pivot = n - 2 #2nd to last pos of nums so nums[pivot] < nums[pivot + 1]
    while pivot >= 0 and nums[pivot] >= nums[pivot+1]:  
        
        pivot -= 1  #decrements pivot to reindex for the next swap for lexicographic

    if pivot == -1: #last permutation has been found
        return False


    j = pivot + 1 #set j index in front pivot
    while j < n and nums[pivot] < nums[j]: #finds next largest j to pivot
        j += 1
    j -= 1
    nums[pivot], nums[j] = nums[j], nums[pivot] #swap pivot j to j pivot
    l = pivot + 1 #beginning of rest of list after nums[i]
    r = n - 1 #end of rest of list
    while l < r:

        #reverses order of remaining list to get into correct order for lexico
        nums[l], nums[r] = nums[r], nums[l]
        
        l += 1
        r -= 1
    return True

   
def main():
    #generates list to be passed in5to fxn
    nums = []
    count = 0
    n = int(input("What number 1-9 would you like for n? ")) #length chosen for nums
    i = 1
    #creates list 1 to n
    while i <= n:
        nums.append(i)
        i += 1
    #while perm generator returns true keeps going once false last perm has been reached
    while True:
        print(nums)
        count += 1
        if not permutation_gen(nums):
            print("permutations ", count)
            break
if __name__ == "__main__":
    main()

    


 






