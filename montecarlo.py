import random

def miller(num, base):

    if num % 2 == 0:
        return False
    
    # write n-1 as 2**k * newnum
    # repeatedly try to divide n-1 by 2
    k = 0
    newnum = num-1

    while True:
        quotient, remainder = divmod(newnum, 2)
        if remainder == 1:
            break
        k += 1
        newnum = quotient

    if pow(base, newnum, num) == 1:
        return True
            
    for i in range(k):
        if pow(base, newnum, num) == num-1:
            return True
        else:
            newnum *= 2
    return False
 
def trial_runs(num):
    trials = 10
        
    for i in range(trials):
        base = random.randrange(2, num)
        prob_prime = miller(num,base)
        
        if prob_prime is False:
            return False
    return True

def test_prime(n):
    if n == 2 or n == 3: return True
    if n < 2 or n%2 == 0: return False
    if n < 9: return True
    if n%3 == 0: return False
    r = int(n**0.5)
    f = 5
    while f <= r:
       
        if n%f == 0: return False
        if n%(f+2) == 0: return False
        f +=6
    return True   



def main():
    count = 0
    
    for i in range(3, 1000000):
     
        if test_prime(i) != trial_runs(i):
            count += 1
         
    print("There was {} matches.".format(count))

main()


